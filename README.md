# Motto: **_Centrifuge CANNOT Rest_**

# Topic

### Unsaturated Soil Triaxial Testing DEM Modelling

# Target

#### To simulate unsaturated soil triaxial behavior based on linear model and nonlinear modeling.

##### To give in-deep analysis of soil information, such as local strain analysis and ???

# Plan and DDL

#### 1. 5000 particles saturated modelling based on linear model, 17, 34, 50, 100, and 200 kPa.
- [ ] Dec 23
#### 2. Let office computer calculate different contact angle including??
- [ ] Dec 26
#### 3. 5000 particles unsaturated modelling based on a high suction effect result.
- [ ] Dec 27
#### 4. high-order micro-scope analysis of modelling. First try.
- [ ] One week
#### 5. step 1 but nonlinear model
- [ ] Dec 30
#### 6. try to find reasonable parameters
- [ ] Jan 2
#### 7. parameter analysis and similarity analysis


# What's more?
#### How to use HPC?

