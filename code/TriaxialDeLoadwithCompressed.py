# -*- encoding=utf-8 -*-

# parameters
readParamsFromTable(
	sigma = 50e3,
	MaxDmm=0.5,  # maximum particle diameter
	MinDmm=0.25,  # minimum particle diameter
	NumParticle=2000,
	compFricDegree = 30, # contact friction during the confining phase
	targetPorosity = 0.350,
	rate =0.01
)
from yade.params import table
from yade import pack,plot,qt
key = f'{table.MinDmm}-{table.MaxDmm}mm{int(table.sigma)}kpa{table.NumParticle}p0.349'

O.load(f'../Compression/{key}compressiondaily.yade')
setContactFriction(radians(table.compFricDegree))

rate = 0.01
young = 5e6		# Young's Modulus
poisson=.4		# Poisson ratio
density=2680
damp=0.2
stabilityThreshold = 0.001
gravity=(0,-9.81,0)
triaxMaxStrain =0.25

triax=TriaxialStressController(
	thickness = 0,
	stressMask = 5,
	internalCompaction=False,
	goal1=-table.sigma,
	goal2=-rate,
	goal3=-table.sigma,
	max_vel=0.01,
)

O.engines=[
	ForceResetter(),
	InsertionSortCollider([Bo1_Sphere_Aabb(),Bo1_Box_Aabb()]),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_FrictPhys()],
		[Law2_ScGeom_FrictPhys_CundallStrack()]
	),
	GlobalStiffnessTimeStepper(timeStepUpdateInterval=10,label='GST'),
	triax,
	NewtonIntegrator(damping=damp,label='newton'),
	PyRunner(command='StDeColor()',iterPeriod=200),
	PyRunner(command='PT()',iterPeriod=100,label='checkdt'),
	PyRunner(command='Checker()',iterPeriod=100,label='checker'),
	# VTKRecorder(iterPeriod = 100, recorders=['spheres','boxes'],fileName='p1-'),
]

# Gl1_Sphere.stripes=1
# yade.timing.stats()

def StDeColor():
	for Bo in O.bodies:
		if isinstance(Bo.shape, Sphere):
			Bo.shape.color = blue2red(Bo.state.rot().norm(),0,pi)

def Checker():
	if unbalancedForce() < stabilityThreshold and triax.strain[2] > triaxMaxStrain:
		O.pause()
		print("Finish.")
		Saver()


def Saver(suffix=suffix):
		if 1:
			TIME = time.strftime('_%y%m%d_%H%M')
			P = format(triax.porosity,'.3f')
			O.save(f'../Saturated/{key}SatTriax{suffix}{TIME}.yade')
			print("Saved.")
			plot.saveDataTxt(f'../Saturated/{key}SatTriax{suffix}{TIME}data.txt')

def PT():
	plot.addData(e11=-triax.strain[0], e22=-triax.strain[1], e33=-triax.strain[2],
				 ev=-triax.strain[0] - triax.strain[1] - triax.strain[2],
				 s11=-triax.stress(triax.wall_right_id)[0],
				 s22=-triax.stress(triax.wall_top_id)[1],
				 s33=-triax.stress(triax.wall_front_id)[2],
				 i=O.iter,
				 t=O.time,
				 unb = unbalancedForce(),
				 porosity = triax.porosity
				 )

def blue2red(x,xmin,xmax):
	xnorm = min(1.,max((x-xmin)/(xmax-xmin),0.))
	return (xnorm,1-xnorm,1-xnorm)

StDeColor()
qt.View()
plot.plots={'e22':('s11','s22','s33'),'e22 ':'ev', 'i':'unb','i ':'porosity',}


plot.plot()
#Save temporary state in live memory. This state will be reloaded from the interface with the "reload" button.
O.saveTmp()
# O.run()

waitIfBatch()