###############################################
# This simulation is for verifing Meidani,2017
###############################################


#setting some parameters that may change
readParamsFromTable(
	sigma = 10e3, # confining stress, Pa
	compFricDegree = 35, # contact friction during the confining phase, degree
	targetPorosity = 0.396,
	su = 5e3 #suction, Pa
)

from yade.params import table	# for read params from table
from yade import pack,plot,qt

Particles='PSDParticles5000SpCreate_220816_1933.txt' # input particles information from
Fric = table.compFricDegree # store the friction degree for friction reducing method

# input Particles information to SpherePack() and name it as sp
sp = SpherePack()
sp.load(Particles)
mn, mx = sp.aabb()


rate =0.02		# ??

# Particles material information
young = 150e6		# Particle Young's Modulus
poisson=.4		# !!! for Ip2-fricmat-frictmat-frictphys or coh,  poisson means normal/shear stiffness ratio
density=2720	# particle density
damp=0.2		# whose damp?
alphaKr=0.15	# dimensionless rolling stiffness
etaKr=1			# dimensionless rolling stength


stabilityThreshold = 0.001	#
CompressionStep = 1 # for record compression step
sumV = 0	#for calculating liquid volume
step = 0	#
triaxMaxStrain = 0.25

# suction information
EnlargeFactor=1
never=0
surfaceTension=0.073
createDistantMeniscii = 0


# define materials
O.materials.append(FrictMat(
	young=young,
	poisson=poisson,
	frictionAngle=radians(table.compFricDegree),
	density=density,
	label='spheres'))


O.materials.append(FrictMat(young=young,poisson=poisson,frictionAngle=0,density=0,label='frictionless'))

# define aabbwalls from O.bodies[0] to O.bodies[5]
walls=aabbWalls([mn,mx],material='frictionless')
wallIds=O.bodies.append(walls)

sp.toSimulation(material='spheres')

triax=TriaxialStressController(
	thickness = 0,
	stressMask = 7,
	internalCompaction=False,
	goal1=-table.sigma,
	goal2=-table.sigma,
	goal3=-table.sigma,
	max_vel=0.01,)

O.engines=[
	ForceResetter(),
	InsertionSortCollider([Bo1_Sphere_Aabb(aabbEnlargeFactor=EnlargeFactor,label='aabb'),Bo1_Box_Aabb()]),
	InteractionLoop(
		[Ig2_Sphere_Sphere_ScGeom(interactionDetectionFactor=EnlargeFactor,label='SSS'),Ig2_Box_Sphere_ScGeom()],
		[Ip2_FrictMat_FrictMat_CapillaryPhys()],
		[Law2_ScGeom_FrictPhys_CundallStrack(neverErase=neverErase,label='LawSFC')]
	),
	Law2_ScGeom_CapillaryPhys_Capillarity(capillaryPressure=0,surfaceTension=surfaceTension,label='suction',createDistantMeniscii = createDistantMeniscii),
	GlobalStiffnessTimeStepper(timeStepUpdateInterval=10,label='GST'),
	triax,
	NewtonIntegrator(damping=damp,label='newton'),
	PyRunner(command='PT()',iterPeriod=2000,label='checkdt'),
	PyRunner(command='Compressor()',iterPeriod=2000,label='checker'),
]

def Compressor():
	print(triax.porosity)
	if unbalancedForce() < stabilityThreshold and abs(-table.sigma-triax.meanStress)/table.sigma<0.001:
		global CompressionStep,Fric
		CompressionStep += 1
		if CompressionStep < 10000:
			if triax.porosity > table.targetPorosity:
				Fric *= 0.95
				setContactFriction(radians(Fric))
			else:
				print("Compression finished. Porosity is " + str(triax.porosity))
				print("Compression Step is " + str(CompressionStep))
				setContactFriction(radians(table.compFricDegree))
				O.pause()
				print("Finish.")
		else:
			O.pause()
			print("10k times compression finished. Porosity is " + str(triax.porosity))


def PT():
	plot.addData(i=O.iter,
				 t=O.time,
				 porosity = triax.porosity
				 )


plot.plots = {'t':('porosity')}

plot.plot()

import qt
qt.View()

# O.run()
