import numpy as np
d=np.genfromtxt('dataporeFill0_0.xml.bz2',dtype=None,names=True)  
d.shape
d.size
x = []
for i in range(len(d)): 
    x += [d[i][1]] 
from matplotlib import pyplot as plt 
# stress strain curve
plt.figure()
plt.title("stress strain curve") 
plt.xlabel("axial strain") 
plt.ylabel("deviatoric stress") 
plt.plot(d['a_strain'],d['d_stress'],d['a_strain'],d['c_stress'])       
plt.xlim((0,0.35))   
plt.ylim((0,3e6))                            
plt.show()   
#plt.savefig('stress strain curve.jpg')
plt.figure()
plt.title("volumtric strain curve") 
plt.xlabel("axial strain") 
plt.ylabel("volumtric strain") 
plt.plot(d['a_strain'],d['volumtric_strain'])#,d['a_strain'],d['poro'])                                   
plt.show()  


"""
def ShParticleGenerator(sh=0.2,psh = 0.04):
    print("============== generated Sh is ", sh, "==================")
    print("============== the porosity now is  ", porosity(), "==================")
    check_radius = psHyd/2
    nn = 0
    x = 
    r = psHyd/2
    while(porosity() > n):
    for b in O.bodies:
        dist = 
        if (dist > r1 + r2):
            #generated

    print("the saturation of hydrate is ", hvol/(vol-tvol) )
    print("the porosity now is ", (vol-tvol)/(vol) )
    """
